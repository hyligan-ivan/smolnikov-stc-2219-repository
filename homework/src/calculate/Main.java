package calculate;

public class Main {
    public static void main(String[] args) {
        System.out.println(StaticCalculator.sumValues(0, 1, 2, 3, 4, 5));
        System.out.println(StaticCalculator.differenceValues(100,30,5,300));
        System.out.println(StaticCalculator.maxdifferenceValues(10,20,20,100));
        System.out.println(StaticCalculator.multiplicationsValues(2,3,4));
        System.out.println(StaticCalculator.divisionValues(100,2,200,-23));
        System.out.println(StaticCalculator.factorialValues(19));
        StaticCalculator staticCalculator = new StaticCalculator();

    }
}
