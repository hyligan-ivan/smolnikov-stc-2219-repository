package calculate;

import java.math.BigInteger;

public class StaticCalculator {

    private static int x;

    public static int sumValues(int ... values) {
        int result = 0;

        for (int i = 0; i < values.length; i++) {
            result = result + values[i];
        }

        return result;
    }

    public static double differenceValues(int ... values) {
        double result = values[0];
        for (int i = 1; i < values.length;i++){
            result = result - values[i];
        }

        return  result;

    }
    public static double maxdifferenceValues(int ... values) {
        double result = 0;
        double max = 0;
        for (int i = 0; i < values.length;i++){
            if (max > values[i]) {
                result = result+values[i];

            } else{
                result = max+result;
                max = values[i];
            }


        }
        result = max - result;

        return  result;



    }

    public static int multiplicationsValues(int ... values) {
        int result = values[0];

        for (int i = 1; i < values.length; i++) {
            result = result * values[i];
        }

        return result;
    }
    public static double divisionValues(double ... values) {
        double result = values[0];

        for (int i = 1; i < values.length; i++) {
            if (result > values[i] && values[i] > 0){
            result = result / values[i];

            }
        }


        return result;
    }
    public static long factorialValues(int x ){

        long result = 1;
        if( x > 0 && x<20 ){
            result = 1;
            for (int i = 2; i <= x; i++) {
                result = result * i;
            }

        }

        //if (x>20){
        //    //BigInteger result1 = BigInteger.ONE;
        //    for (int i = 2; i <= x; i++)
        //      //  result = result1.multiply(BigInteger.valueOf(i));
        //}
        return result;

    }

}




