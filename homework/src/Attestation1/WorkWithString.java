package Attestation1;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class WorkWithString extends  WorkWithFile {
    public static void main(String[] args) throws IOException {
        String string = WorkWithString.getStringFromFile();

        if(string != null) {
            countingWords(string, 1);
            countingWords(string, 2);
        }



    }

    private static void countingWords(String string, int i) throws IOException {
        Map<String,Integer> words = new HashMap<>();
        string = string.replaceAll("\\p{Punct}", "").toLowerCase();
        if (i == 1){
            String[] info = string.split("\\s+");
            for (String t:info){
                if (words.containsKey(t)){
                    words.put(t, words.get(t)+1);
                } else {
                    words.put(t,1);
                }
            }
            WorkWithFile.updateFile(words,false, "БЕЗ СОРТИРОВКИ");

            Set<String> keys = words.keySet();

            Map<String,Integer> sortedMap= aramge(words);
            WorkWithFile.updateFile(sortedMap,true, "АЛФАВИТНЫЙ ПОРЯДОК");
        } else if (i == 2) {
            //убрал пробелы
            String charsToRemove = " ";
            for (char c:charsToRemove.toCharArray()){
                string = string.replace(String.valueOf(c), "");
            }
            char[] chars = string.toCharArray();
            for (char ch:chars){
                if (words.containsKey(String.valueOf(ch))){
                    words.put(String.valueOf(ch), words.get(String.valueOf(ch))+1);

                } else {
                    words.put(String.valueOf(ch),1);
                }
            }
            Set<String> keys = words.keySet();

            WorkWithFile.updateFile(words,true,"БЕЗ СОРТИРОВКИ");
            //@GamesIS Изменил название метода с маленьекой буквы
            Map<String,Integer> sortedMap= aramge(words);
            //@GamesIS Изменил название метода с маленьекой буквы
            WorkWithFile.updateFile(sortedMap, true,"АЛФАВИТНЫЙ ПОРЯДОК");

        }
    }

        private static Map<String, Integer> aramge(Map<String, Integer> words) {
            Map<String, Integer> sortedMap =words.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (a, b) -> { throw new AssertionError(); },
                            LinkedHashMap::new
                    ));
            return sortedMap;

        }



}
