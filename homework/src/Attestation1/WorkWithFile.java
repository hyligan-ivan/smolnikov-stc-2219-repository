package Attestation1;

import java.io.*;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

public class WorkWithFile {
 /*   public static void main(String[] args) throws IOException {
        File file = new File("homework/txt.txt");
        try (Writer writer = new FileWriter(file, true)) {
            file.createNewFile();
            writer.write("Иезекииль Булыжников 33 true\n");
            writer.write("2 Василий Камушкин 23 false\n");
            writer.write("3 Василь Камушкин 23  true\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    } */
    static void updateFile(Map<String, Integer> words, boolean append, String q) throws IOException {
        //@GamesIS int i заменил на append
        //Названия переменных должны нести смысловую нагрузку
        try (Writer writer = new FileWriter("result.txt", append)) {
            Set<String> keys = words.keySet();
            writer.write("      "+q+"\n");
            for (String key:keys){

                //System.out.print(key+" = "+words.get(key)+";  ");
                writer.write(key+" = "+words.get(key)+";  " + "\n");
                //System.out.println("Writer");
            }

        } catch (IOException exception) {
            File file = new File("result.txt");
            file.createNewFile();
            throw new RuntimeException();
        }



    }

    public static String getStringFromFile() {



        String StringBuilder = null;
        //@GamesIS используйте camelCase для названий переменных и функций
        String FileName;
        //System.out.println("Введите путь к Файлу //по умолчанию 1 [txt.txt]");
        System.out.println("Пример: txt.txt");
        //String path = "homework/txt.txt";
        //@GamesIS не нужная  переменная
        //String basePath = new File("").getAbsolutePath();
        //System.out.println(basePath);
        ///String path = this.getClass().getClassLoader().getResource()
        System.out.print("Введите путь к Файлу //по умолчанию 1 [src/Attestation1/txt.txt] :");
        //@GamesIS Изменил путь К файлу
        Scanner scanner = new Scanner(System.in);
        FileName = scanner.nextLine();
        if (Objects.equals(FileName, "1")){
            FileName ="src/Attestation1/txt.txt";
            //System.out.pri0ntln(FileName);
        }
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FileName))){
            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                line = line.replace("[\\s]{2,}", " ");
                //String[] info = line.split(" ");
                //Изменил @GamesIS
                if (StringBuilder == null){
                    StringBuilder = line;
                } else {
                    StringBuilder = StringBuilder+" "+line;
                }
                // System.out.println(AllText);

            }
        } catch (IOException e) {
            System.out.println("Не найден файл, попрбать снова? 1 - да 2 нет");
            Scanner scanner1 = new Scanner(System.in);
            int id = scanner1.nextInt();
            if (id == 1){getStringFromFile();}
            else  { throw new RuntimeException();}

        }
        return StringBuilder;

    }

}
