package homework4;

public class Rectangle extends Figure {
    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public int getPerimeter() {
        int perimeter = (this.x + this.y) * 2;
        System.out.println("Периметр Прямоугольника = " + perimeter);
        return  perimeter;

    }


}
