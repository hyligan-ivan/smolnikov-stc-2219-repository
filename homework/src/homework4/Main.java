package homework4;

public class Main {
    public static void main(String[] args){

             
        Figure[] figure = new Figure[4];
        figure[0] = new Rectangle(10,30);
        figure[1] = new Square(20,4);
        figure[2] = new Ellipse(1,10);
        figure[3] = new Circle(2,0);

        for (int i = 0; i < figure.length; i++){
            figure[i].getPerimeter();

            if (figure[i] instanceof Moveable){
                ((Moveable) figure[i]).move(getRandomNumber(),getRandomNumber());
            }
            System.out.println("___________________");
        }

    }
    static int getRandomNumber()
    {
        return (int) (Math.random() * 100);

    }
}
