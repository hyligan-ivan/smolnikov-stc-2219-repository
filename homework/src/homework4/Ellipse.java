package homework4;

import static java.lang.Math.*;

public class Ellipse extends Figure{

    public Ellipse(int x, int y) {
        super(x, y);
    }

    //Math. * Math.sqrt((getX()*getX()+getY()*getY())/2);
    @Override
    public int getPerimeter() {
        double perimeter = 2*Math.PI*sqrt((this.x * this.x + this.y * this.y) / 2);
        System.out.println("Периметр элипса = " + perimeter);
        return (int) perimeter;


    }
}
