package homework4;

public class Circle extends Ellipse implements Moveable{



    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public int getPerimeter() {
        double perimeter = 2*Math.PI*this.x;
        System.out.println("Периметр круга = " + perimeter);
        return (int) perimeter;
    }


    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Случайныые координаты центра круга x=" + x+" y="+y);


    }

    static int getRandomNumber()
    {
        return (int) (Math.random() * 100);

    }

}
