package homework4;

public class Square extends Rectangle implements Moveable{

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public int getPerimeter() {
        int perimeter = 4*this.x;
        System.out.println("Периметр квадрата = " +perimeter);
        return (int) perimeter;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Случайныые координаты центра квадрата x=" + x+" y="+y);


    }

    static int getRandomNumber()
    {
        return (int) (Math.random() * 100);

    }
}
