package homework7;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class WorkWithFile {
    public static void main(String[] args) {
        File file = new File("staff.txt");
        try (Writer writer = new FileWriter(file, true)) {
            file.createNewFile();
            writer.write("1|Иезекииль|Булыжников|33|true\n");
            writer.write("2|Василий|Камушкин|23|false\n");
            writer.write("3|Василь|Камушкин|23|true\n");
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}
