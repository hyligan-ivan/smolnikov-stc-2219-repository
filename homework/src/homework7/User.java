package homework7;

public class User {
    private int id;
    private String name;
    private String lastname;
    private int age;
    private boolean work;


    public User(int id, String name, String lastname, int age, boolean work) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.work = work;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    public String toString(){
        return id + "|"
                + name + "|"
                + lastname + "|"
                + age + "|"
                + work;
    }

}
