package procfunction;

public class IndexArray {
    public static void main(String[] args) {
        int[] array = {100, 16, 0, 10, 145};
        int number = 145;
        int result = enumeration(array,number);
        System.out.println("'Элемент в массиве = " + result);

    }
    public static int enumeration(int[] array, int number) {
        int num = -1;
        for (int i = 0; i < array.length; i++ ){
          if (array[i] == number){
              num = i;
          }
        };
        return num;

    }
}
