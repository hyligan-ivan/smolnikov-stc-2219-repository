package homework6;
import java.util.ArrayList;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] list = new int[array.length];
        int x = 0;
        for (int i=0;i< array.length;i++) {
            if (condition.isOK(array[i])) {
                list[x]  = (array[i]);
                x++;
            }

        }
        return list;
    }
}
